function [ y,t ] =disp_PI(num,den)
%用来显示PI整定结果
       t=0:0.01:30;
       sys=tf(num,den);
       y=step(sys,t);
       grid on;                                              %画网格%选择满足要求的积分时间
       axis([0 30 0 1.5]);
end

