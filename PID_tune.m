function [Eva_PID,ZeTer_PID,I_PID,D_PID,Tr_PID,Ts_PID] = PID_tune(Zp,TTr,num0,den0)
%PID系数整定
   ZeTer_PID=0.8*Zp;          %按照衰减曲线法将比例度变为0.8倍
   I_PID=0.3*TTr;             %将积分时间变为0.3倍的衰减时间
   D_PID=0.1*TTr;             %将微分时间变为0.1倍的衰减时间
   K2=(1/ZeTer_PID*I_PID*D_PID);
   K1=(1/ZeTer_PID)*I_PID;
   K0=(1/ZeTer_PID);
   sys1_PID=tf([K2,K1,K0],[I_PID,0]);
   [num_PID,den_PID]=GouZaotf(sys1_PID,num0,den0);%待整定传递函数系数
   %调用DongTaiZhiBiao函数，返回上升时间Tr、调节时间Ts、衰减比sjb和静差
   [Tr_PID,Ts_PID,~,~,JingCha_PID]=DongTaiZhiBiao(num_PID,den_PID);
   %调用输出函数，输出性能指标并画出单位阶跃响应曲线
   Eva_PID=Tr_PID+Ts_PID;
end

