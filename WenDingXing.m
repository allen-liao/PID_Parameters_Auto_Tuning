function [ k ] = WenDingXing(num,den)
%通过判断系统的闭环极点是否都在复平面的左半平面来判断系统的稳定性
%用0-1表示系统稳定性，0-不稳定，1-稳定   
     G=tf(num,den);             %求系统开环传递函数
     root=roots(G.den{1});      %求闭环系统的根
     n=length(root);            %计算根的个数
    for i=1:n
     if real(root(i))>=0
          k=0;                  %系统不稳定 
     else k=1;                  %系统稳定
     end
    end    
end